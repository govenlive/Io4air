package consts
{
	import flash.display.BitmapData;

	public class LiveBitmapdate
	{
		public static const JPEG:String=".jpg";
		public static const PNG:String=".png";
		public static const JPEG_XR:String=".wpd";
		public var quality:Number=80;
		public var bitmapdate:BitmapData
		public var form:String
		public function LiveBitmapdate()
		{
		}
	}
}