package air.live
{
      import flash.filesystem.File;
      
     
      /**
       *  @data 2012-3-19
       *  @author 偷心枫贼
       *  @mail 527025965@qq.com
       *  @qq 527025965
       *
       *	遍历文件夹
       *  @see  
       *  @langversion 3.0  
       *  @playerversion AIR 2.6以上 
       *  @productversion v1.0
       *  @example <listing version="3.0">(这里写代码样式)</listing>
       *
       **/
      public class LiveTraverFolder
      {

		  
	     
		  private static var instance:LiveTraverFolder
		  public static function getInstance():LiveTraverFolder{
			  if (!instance) { instance = new LiveTraverFolder()}
			  return instance	  
		  }
		  private var _file:File
		  
		  /**
		   * 遍历文件夹内的文件 
		   * @param $suffixs 格式条件 例如:[".jpg",".png",".xml"]
		   * @param $folder 文件夹
		   * @param $pathto 
		   * 
		   */                     
		  public function LiveTraverFolder()
		  {        
			
		  }
		  
		  /**
		   * 从 应用程序已安装文件的文件夹 获取
		   * @param path
		   * @return 
		   * 
		   */	
		  
		
		  public static var depth:uint=0;
		  public static var isOne:Boolean=true;
		  
		  
		  public function get allArr():Vector.<FolderData>{return _allArr}
		  private var _allArr:Vector.<FolderData>
		  
		 
		  public function getFrom_ApplicationDirectoryList(path:String=null):Array{ 
			  if(path==null){
				  _file= File.applicationDirectory
			  }else{
				  _file= File.applicationDirectory.resolvePath(path)
			  }
			  

			  process()  
			  return newArr
		  }
		  public function getFrom_Other(path:String=null):Array{ 
			  if(path==null){
				  _file= File.applicationDirectory
			  }else{
				  _file= new File(path);
			  }
			  
			 
			  
			  process()  
			  return newArr
		  }
		  
		  /**
		   * 从应用程序的专用存储目录获取。 
		   * @param path
		   * @return 
		   * 
		   */		  
		  public function getFrom_ApplicationStorageDirectoryList(path:String=null):Array{
			  if(path==null){
				  _file= File.applicationDirectory
			  }else{
				  _file = File.applicationStorageDirectory.resolvePath(path)
			  }
			  
			
			  process()  
			  return newArr
		  }
		  
		  /**
		   * 从桌面获取 
		   * @param path
		   * 
		   */		
		  public function getFrom_desktopDirectory(path:String=null):Array{
			
			  if(path==null){
				  _file= File.applicationDirectory
			  }else{
				  _file=File.desktopDirectory.resolvePath(path)
			  }
			  process()  
			  return newArr
		  }
		  /**
		   * 从我的文档读取 
		   * @param path
		   * 
		   */		
		  public function getFrom_documentsDirectory(path:String=null):Array{
			  if(path==null){
				  _file= File.applicationDirectory
			  }else{
				  _file=File.documentsDirectory.resolvePath(path)
			  }
			
			  process()  
			  return newArr
		  }
		  
		  
		  
		  
		  
		  
		  
		 private  var newArr:Array=[]
	     private function process():void{
			// var fold:FolderData= new FolderData()
			 _allArr=new Vector.<FolderData>();
				
				depth=0
			  pop(_file,_allArr)
			//  trace("length: "+_allArr.length)
			 if(isOne&&_allArr.length>0){
				 newArr=_allArr[0].fileArr
			 }
		 }
		 
		
		
		  
		  public static var suffixes:Array=['.jpg','.png'];
		  
		  
		  
		  
		  public var isOnlyFileName:Boolean=false
		  
		  private function pop(file:File,folderArr:Vector.<FolderData>,title:String="",n:int=0):void{
			  if(file.isDirectory){
				  var arr:Array= file.getDirectoryListing();	
				  
				  var fileData:FolderData=new FolderData()

				 fileData.title=file.name
			//	trace(fileData.title)
				
				
				  
				//  fileData.title=title
				  for each(var filetemp:File in arr){
					 var urltemp:String=filetemp.nativePath
						 
				//	trace(filetemp.nativePath,filetemp.name)
					
					  if(!filetemp.isDirectory)  
					  {  
						  var _suffix:String=urltemp.slice(-4,Number(urltemp.length))
						  
						  if(LiveTraverFolder.suffixes.indexOf(_suffix)!=-1){
							
							  fileData.fileArr.push(urltemp)
						  }	
					  }
					  else
					  {
						  if(LiveTraverFolder.isOne){
							  var folderTempArr:FolderData=new FolderData()
							 folderArr.push(folderTempArr)
							  pop(filetemp, folderArr,urltemp,n+1)
						  }
						  
						  if(LiveTraverFolder.depth<n)LiveTraverFolder.depth=n
						  
					  }
					  
				  }
				  
				  
				  folderArr.push(fileData)
			  }
		  }
		
      }
}