package air.goven
{
	import flash.filesystem.File;
	import flash.utils.ByteArray;

	public interface IFileLive
	{  
		function get isCompress():Boolean
		function set isCompress(value:Boolean):void
		
		
		
		
		function write(obj:Object,path:String,file:File):Boolean
		function writeAsync(obj:Object,path:String,file:File):void
		
		function readObject(path:String,file:File):Object
		function readJson(path:String,file:File):String
		function readXML(path:String,file:File):XML
		function readByte(path:String,file:File):ByteArray
		
	   // function readAsync(path:String,file:File):void
	}
}