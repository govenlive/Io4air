package air.goven
{
	import flash.filesystem.File;

	public class FileType
	{
		public static const APPLICATION_DIRECTORY:File=File.applicationDirectory
		public static const APPLICATION_STORAGE_DIRECTORY:File=File.applicationStorageDirectory;
		public static const DESKTOP_DIRECTORY:File=File.desktopDirectory;
		public static const DOCUMENTS_DIRECTORY:File=File.documentsDirectory
			
		public static const FILE:File=new File();
			
	}
}