package air.goven
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	public class FileLive implements IFileLive
	{
		private var _isCompress:Boolean=false;
		private var _stream:FileStream=new FileStream();
		private var _file:File
		
		/**
		 * 是否启用压缩,默认不压缩 
		 * @return 
		 * 
		 */		
		public function get isCompress():Boolean
		{
			return _isCompress
		}		
		public function set isCompress(value:Boolean):void
		{
			_isCompress=value
		}
		
		
		public function FileLive()
		{
			
		/*	_stream.addEventListener(Event.COMPLETE, completeHand);
			_stream.addEventListener(IOErrorEvent.IO_ERROR,errorHand);
			_stream.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS,outputHand)
			_stream.addEventListener(Event.CLOSE,closeHand)*/
				
				
		}
		
		
		
		public function write(obj:Object, path:String, file:File):Boolean
		{
			_file=file.resolvePath(path);
			
			
			return false;
		}
		
		
		
		
		
		public function writeAsync(obj:Object, path:String, file:File):void
		{
		}
		
		
		/**
		 * 读取 Object 
		 * @param path
		 * @param file
		 * @return 
		 * 
		 */		
		public function readObject(path:String, file:File):Object
		{
			_file=file.resolvePath(path);
			var data:ByteArray=readData();
			try
			{
				var json:String=String(data);
				return JSON.parse(json);
			} 
			catch(error:Error) 
			{
				
			}
			
			return null;
		}
		
		
		
		/**
		 * 读取 json 
		 * @param path
		 * @param file
		 * @return 
		 * 
		 */		
		public function readJson(path:String, file:File):String
		{
			var data:ByteArray=readData();
			try
			{
				return String(data);
			} 
			catch(error:Error) 
			{
				
			}
			
			return null;
		}
		
		
		/**
		 * 读取XML 
		 * @param path
		 * @param file
		 * @return 
		 * 
		 */		
		public function readXML(path:String, file:File):XML
		{
			_file=file.resolvePath(path);
			var data:ByteArray=readData();
			try
			{
				var xml:XML=new XML(data);
			} 
			catch(error:Error) 
			{
				
			}
			
			
			return null;
		}
		
		
		/**
		 * 读取二进制 
		 * @param path
		 * @param file
		 * @return 
		 * 
		 */		
		public function readByte(path:String, file:File):ByteArray
		{
			_file=file.resolvePath(path);
			return readData();
		}
		
		
		
		/////////////////////////////////////
		
		
		private function readData():ByteArray{
			var data:ByteArray=new ByteArray();
			data.length=0;//初始化
			if(!_file.exists) return null;//文件或目录不存在，直接返回
			try
			{
				_stream.open(_file,FileMode.READ);//读取文件
				_stream.readBytes(data); 
				_stream.close();
				return data;
			} 
			catch(error:Error) 
			{
				
			}
			return null
		}
		
		//写入
		private function write(obj:*):void{
			/*_nativePath=file.nativePath
			_url=file.url
			_data.length=0
			if(obj is String) _data.writeUTFBytes(String(obj));  //字符串
			if(obj is XML) _data.writeUTFBytes(String(xml_header(XML(obj)))); //XML
			if(obj is ByteArray) _data=ByteArray(obj);//bye
			if(obj is LiveBitmapdate) {
				var liveBitmap:LiveBitmapdate=obj as LiveBitmapdate
				var bitm:BitmapData=liveBitmap.bitmapdate
				switch(liveBitmap.form)
				{
					case LiveBitmapdate.JPEG:
					{
						bitm.encode(bitm.rect,new JPEGEncoderOptions(liveBitmap.quality),_data);
						break;
					}
						
					case LiveBitmapdate.PNG:
					{
						bitm.encode(bitm.rect,new PNGEncoderOptions(),_data);
						break;
					}
						
					case LiveBitmapdate.JPEG_XR:
					{
						bitm.encode(bitm.rect,new JPEGXREncoderOptions(),_data);
						break;
					}
						
					default:
					{
						break;
					}
				}
				
			}else{
				if(is_compress) _data.compress();
			}
			
			
			
			if(_is_async){
				stream.openAsync(new File(file.nativePath),FileMode.WRITE);
				stream.writeBytes(_data,0,_data.length)  					
			}else{
				stream.open(new File(file.nativePath),FileMode.WRITE);
				stream.writeBytes(_data,0,_data.length)
				stream.close();
			} */
		}
	}
}