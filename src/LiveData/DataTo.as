package LiveData
{
	import flash.display.BitmapData;
	import flash.display.JPEGEncoderOptions;
	import flash.display.JPEGXREncoderOptions;
	import flash.display.PNGEncoderOptions;
	import flash.utils.ByteArray;
	
	public class DataTo
	{
		public function DataTo()
		{
			
		}
		
		
		private static var instance:DataTo
		public static function getInstance():DataTo{
			if (!instance) { instance = new DataTo()}
			return instance	  
		}
		/**
		 * 二进制转字符串 
		 * @param bye
		 * @return 
		 * 
		 */		
		public function turn_ByteArray_to_String(bye:ByteArray):String{
			try
			{
				bye.uncompress()
			} 
			catch(error:Error) 
			{
				
			}
			return String(bye)
		}
		
		/**
		 * 二进制转XML 
		 * @param bye
		 * @return 
		 * 
		 */		
		public function turn_ByteArray_to_XML(bye:ByteArray):XML{
			try
			{
				bye.uncompress()
			} 
			catch(error:Error) 
			{
				
			}
			return XML(bye)	
		}
		/**
		 * 位图转二进制（JPEG） 
		 * @param bitmapData
		 * @return 
		 * 
		 */		
		public  function turn_BitmapData_to_ByteArrayOfJPEG(bitmapData:BitmapData):ByteArray{
			var byes:ByteArray=new ByteArray()
			var bitm:BitmapData=bitmapData
			bitm.encode(bitm.rect,new JPEGEncoderOptions(100),byes);
			return byes
		}
		/**
		 * 位图转二进制（JPEG_XR） 
		 * @param bitmapData
		 * @return 
		 * 
		 */		
		public function turn_BitmapData_to_ByteArrayOfJPEG_XR(bitmapData:BitmapData):ByteArray{
			var byes:ByteArray=new ByteArray()
			var bitm:BitmapData=bitmapData
			bitm.encode(bitm.rect,new JPEGXREncoderOptions(),byes);
			return byes
		}
		/**
		 * 位图转二进制（PNG） 
		 * @param bitmapData
		 * @return 
		 * 
		 */		
		public function turn_BitmapData_to_ByteArrayOfPNG(bitmapData:BitmapData):ByteArray{
			var byes:ByteArray=new ByteArray()
			var bitm:BitmapData=bitmapData
			bitm.encode(bitm.rect,new PNGEncoderOptions(),byes);
			return byes
		}
		
		public function turn_object_to_string(obj:Object):String{
			return JSON.stringify(obj)	
		}
		
		
		public function turn_xml_to_object(xml:XML):Object{
			var returnObj:Object=new Object(); 
			walkXML(returnObj,xml); 
			function walkXML(obj:Object,xml:XML):void{ 
				for each (var subXML:XML in xml.elements()) {					
					obj[subXML.name()]=new Object(); 
				   
				//	writeObj(obj[subXML.name()],subXML); 
					trace(subXML.name()+" : "+subXML)
					walkXML(obj[subXML.name()],subXML); 
				} 
			} 
			function writeObj(obj:Object,xml:XML):void{ 
				for each (var attribute:XML in xml.attributes()) { 
					
					var dename:String=String(attribute.name()) 
					obj[dename]=attribute.toString(); 
				} 
			} 
			return returnObj; 
		}
		
	} 
	
}
